#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2023] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Move Layer
# An Inkscape 1.2.1+ extension
# Appears under Extensions>Arrange>Move Layer
##############################################################################

import inkex

import gi

# This statement to sent Gtk3 stderr away :)
import sys
tmp, sys.stderr = sys.stderr, None  # type: ignore

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GdkPixbuf

from time import time

# import warnings
# warnings.filterwarnings("ignore")

# -------------------------------------------------------------------------------
# HANDLERS
# -------------------------------------------------------------------------------

class Handler:
    def on_destroy(self, *args):
        Gtk.main_quit()

    def apply_button(self, *args):

        # Get user selected export type
        selection_type_radio_group = builder.get_object('move_type_move_rb').get_group()
        for radio in selection_type_radio_group:
            if radio.get_active():
                selection_type = radio.get_name().replace('_rb', '')

        # Get insertion order
        insertion_order_radio_group = builder.get_object('insert_location_first_rb').get_group()
        for radio in insertion_order_radio_group:
            if radio.get_active():
                insertion_order = radio.get_name().replace('_rb', '')

        layer_treeview = builder.get_object("layer_treeview")

        tree_selection = layer_treeview.get_selection()
        model, treeiter = tree_selection.get_selected()

        if treeiter is not None:
            row_index = model.get_path(treeiter)
        else:
            global global_msg_buffer
            global_msg_buffer.set_text('No Target Layer Selected')
            return

        target_layer_id = model[row_index][1]

        move_copy_layer(inkex_self, target_layer_id, selection_type, insertion_order)

        Handler.on_destroy(None)


# -------------------------------------------------------------------------------
# END OF HANDLERS
# -------------------------------------------------------------------------------


# -------------------------------------------------------------------------------
# MAIN GTK3 LOOP
# -------------------------------------------------------------------------------

def run_gtk():
    # Load stylesheet
    screen = Gdk.Screen.get_default()
    provider = Gtk.CssProvider()
    provider.load_from_path("move_layer.css")
    Gtk.StyleContext.add_provider_for_screen(screen, provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

    global builder
    builder = Gtk.Builder()

    builder.add_from_file('move_layer.glade')

    global window
    window = builder.get_object("main_window")
    builder.connect_signals(Handler())
    window.connect("destroy", Gtk.main_quit)
    window.show_all()

    global msg_textview
    msg_textview = builder.get_object("msg_textview")
    global global_msg_buffer
    global_msg_buffer = Gtk.TextBuffer()
    global_msg_buffer.set_text('Choose a Target Layer')
    msg_textview.set_buffer(global_msg_buffer)

    global current_layer_gtk3_label
    current_layer_gtk3_label = builder.get_object("current_layer_label")

    # Set min height and width for scrolled window ( layer window )
    layer_scrolled_window = builder.get_object("layer_scrolled_window")
    # Values below can be changed to change size of scrolled layer window
    min_content_width = 300
    min_content_height = 300

    layer_scrolled_window.set_min_content_width(min_content_width)
    layer_scrolled_window.set_min_content_height(min_content_height)


    # Populate the treeview

    layer_list = inkex_self.svg.xpath('//svg:g[@inkscape:groupmode="layer"]')

    create_layer_liststore(inkex_self, layer_list)

    Gtk.main()

# -------------------------------------------------------------------------------
# END OF MAIN GTK3 LOOP
# -------------------------------------------------------------------------------

# -------------------------------------------------------------------------------
# START OF USER FUNCTIONS
# -------------------------------------------------------------------------------

def create_layer_liststore(self, layer_list):

    current_layer = inkex_self.svg.get_current_layer()
    current_layer_id = current_layer.get_id()
    current_layer_label = current_layer.get('inkscape:label')
    current_layer_gtk3_label.set_text(f'Label : {current_layer_label}\nId : {current_layer_id}')

    parent_layer = current_layer.getparent()

    sub_layers = current_layer.xpath('descendant::svg:g[@inkscape:groupmode="layer"]')

    layer_liststore = Gtk.ListStore(str, str)
    layer_label_id_list = []
    for layer in layer_list:
        label = layer.get('inkscape:label')
        id = layer.get_id()
        if not label:
            label = id

        if id != current_layer.get_id() and layer not in sub_layers and layer != parent_layer:
            layer_label_id_list.append([label, id])

    for row in layer_label_id_list:
        layer_liststore.append(row)

    global builder
    layer_treeview = builder.get_object("layer_treeview")
    layer_treeview.set_model(layer_liststore)


def apply_layer_transforms(self, layer, target_layer):
    # Apply layer parent composed transform

    parent_transform_cb = builder.get_object("parent_transform_cb")
    if parent_transform_cb.get_active():
        # inkex.errormsg('Correct parent')
        layer.transform = layer.transform @ layer.getparent().composed_transform()
    ######################################
    # Apply the inverse target layer transform
    target_transform_cb = builder.get_object("target_transform_cb")
    if target_transform_cb.get_active():
        # inkex.errormsg('Correct target')
        layer.transform = -target_layer.composed_transform() @ layer.transform
        # layer.transform = layer.transform @ -target_layer.composed_transform()


def move_copy_layer(self, target_layer_id, operation='move', insertion_order='first'):

    target_layer = inkex_self.svg.getElementById(target_layer_id)

    current_layer = inkex_self.svg.get_current_layer()
    current_layer_id = current_layer.get_id()

    if operation == 'move':
        move_layer = current_layer

    elif operation == 'copy':
        move_layer = current_layer.duplicate()
        move_layer.set('inkscape:label', current_layer_id + '_dupe_' + str(time()))

    else:
        move_layer = current_layer.duplicate()
        move_layer.set('inkscape:label', current_layer_id + '_dupe_shell_' + str(time()))
        duplicate_layer_sub_layers = move_layer.xpath('descendant::svg:g[@inkscape:groupmode="layer"]')
        for sublayer in duplicate_layer_sub_layers:
            sublayer.delete()


    apply_layer_transforms(self, move_layer, target_layer)

    if len(target_layer.getchildren()) > 0:
        if insertion_order == 'first':
            target_layer.getchildren()[-1].addnext(move_layer)
        else:
            target_layer.getchildren()[0].addprevious(move_layer)
    else:
        target_layer.append(move_layer)


# -------------------------------------------------------------------------------
# END OF USER FUNCTIONS
# -------------------------------------------------------------------------------


class MoveLayer(inkex.EffectExtension):

    def effect(self):

        global inkex_self
        inkex_self = self

        run_gtk()

        # inkex.errormsg('hello')

if __name__ == '__main__':
    MoveLayer().run()
